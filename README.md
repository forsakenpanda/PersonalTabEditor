# About this Project

The following project is a Personal project for creating a guitar tab editor (similar to Guitar Pro, PowerTab, or TabIt). This project aims to be a very simplified tab editor, 
for the time being. 

## Feature List
The following is a list of features for this project, to completion
* Create and edit a custom tab file
* Use a variety of standard MIDI instruments, including guitars, drums, and piano
* Include real-time media controls for the tab file that is currently being edited (play, pause, stop, etc.)
* Allow multiple 'tracks' 
* Allow a variety of time signatures
* Import MIDI, to create a basic tab file.


## Future Feature List
The following is a list of features that will not count towards the completion of this project, but would be interesting to try and add
* Some form of AI, which can track the user's habits while writing music, to both make tabbing quicker, while also making the user aware of their own style

