﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace GuitarTabEditor
{

    class AudioManager
    {

        //Docs regarding mciSendString, mciGetErrorString, and their respective parameters, can be found at https://msdn.microsoft.com/en-us/library/windows/desktop/dd757151(v=vs.85).aspx
        [DllImport("winmm.dll")]
        private static extern int mciSendString(string Cmd, StringBuilder StrReturn, int ReturnLength, IntPtr HwndCallback);


        [DllImport("winmm.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int mciGetErrorString(int errCode, StringBuilder errMsg, int buflen);

        private bool paused;
        private string fileLocation;
        public AudioManager()
        {
            paused = false;
        }

        public bool play()
        {
            int error;

            if (paused)
            {
                error = mciSendString("resume AudioFile", null, 0, IntPtr.Zero);
            } else
            {
                error = mciSendString("play AudioFile", null, 0, IntPtr.Zero);
            }
            paused = false;
            if (error == 0)
            {
                Console.Write("No Error");
                return true;
            } else
            {
                
                Console.Write("Error Occurred: " + mciGetErrorString(error, null, 0));
                return false;
            }
        }

        public bool stop()
        {
            int error;
            error = mciSendString("stop AudioFile", null, 0, IntPtr.Zero);
            error = mciSendString("seek AudioFile to 0", null, 0, IntPtr.Zero);
            paused = false;
            if (error == 0)
            {
                Console.Write("No Error");
                
                return true;
            }
            else
            {
                Console.Write("Error Occurred: " + error);
                return false;
            }
        }

        public bool pause()
        {
            int error;
            error = mciSendString("pause AudioFile", null, 0, IntPtr.Zero);
            if (error == 0)
            {
                Console.Write("No Error");
                paused = true;
                return true;
            }
            else
            {
                Console.Write("Error Occurred: " + error);
                return false;
            }
        }

        public bool Open(String fileLocation)
        {
            int error;
            this.Close();
            this.fileLocation = fileLocation;
            error = mciSendString("open \""+ fileLocation + "\" alias AudioFile", null, 0, IntPtr.Zero);
             
            if (error == 0)
            {   

                return true;
            } else
            {                
                return false;
            }

        }

        /// <summary>
        /// Closes any currently opened audio file
        /// </summary>
        public void Close()
        {
            
            mciSendString("close AudioFile", null, 0, IntPtr.Zero);
        }

        public string getTrackLength()
        {
            int error;
            StringBuilder buffer = new StringBuilder(128);
            TimeSpan ts = new TimeSpan();
            double length;

            error = mciSendString("set AudioFile time format milliseconds", null, 0, IntPtr.Zero);
            error = mciSendString("status AudioFile length", buffer, buffer.Capacity, IntPtr.Zero);

            length = Convert.ToDouble(buffer.ToString());
            ts = TimeSpan.FromMilliseconds(length);

            return string.Format("{0:D2}:{1:D2}", ts.Minutes, ts.Seconds);


        }
    
        public string getTrackTempo()
        {
            int error;
            StringBuilder buffer = new StringBuilder(128);

            error = mciSendString("status AudioFile tempo", buffer, buffer.Capacity, IntPtr.Zero);

            if (error != 0)
            {
                throw new Exception("Could not obtain tempo from audio file. Error: " + error);
            }

            return buffer.ToString() + " bpm";

        }
    }
}
