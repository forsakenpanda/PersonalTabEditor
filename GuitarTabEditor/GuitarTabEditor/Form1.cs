﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuitarTabEditor
{
    public partial class PersonalTabEditor : Form
    {
        FileManager fm;
        AudioManager am;
        string fileName;
        public PersonalTabEditor()
        {
            InitializeComponent();
            fm = new FileManager();
            am = new AudioManager();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void MidiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fm.openMidi())
            {
                fileLength.Text = am.getTrackLength();
                fileTempo.Text = am.getTrackTempo();
            }
            

        }

        private void playButton_Click(object sender, EventArgs e)
        {
            am.play();
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            am.stop();
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            am.pause();
        }
    }
}
