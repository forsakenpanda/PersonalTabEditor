﻿namespace GuitarTabEditor
{
    partial class PersonalTabEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonalTabEditor));
            this.playMenu = new System.Windows.Forms.ToolStrip();
            this.stopButton = new System.Windows.Forms.ToolStripButton();
            this.playButton = new System.Windows.Forms.ToolStripButton();
            this.pauseButton = new System.Windows.Forms.ToolStripButton();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MidiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TabEditor = new System.Windows.Forms.Panel();
            this.songInfo = new System.Windows.Forms.Panel();
            this.SongLength = new System.Windows.Forms.Label();
            this.FileName = new System.Windows.Forms.Label();
            this.tempo = new System.Windows.Forms.Label();
            this.fileLength = new System.Windows.Forms.Label();
            this.fileTempo = new System.Windows.Forms.Label();
            this.playMenu.SuspendLayout();
            this.mainMenu.SuspendLayout();
            this.songInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // playMenu
            // 
            this.playMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stopButton,
            this.playButton,
            this.pauseButton});
            this.playMenu.Location = new System.Drawing.Point(0, 24);
            this.playMenu.Name = "playMenu";
            this.playMenu.Size = new System.Drawing.Size(1904, 25);
            this.playMenu.TabIndex = 0;
            this.playMenu.Text = "toolStrip1";
            // 
            // stopButton
            // 
            this.stopButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stopButton.Image = ((System.Drawing.Image)(resources.GetObject("stopButton.Image")));
            this.stopButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(23, 22);
            this.stopButton.Text = "toolStripButton1";
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // playButton
            // 
            this.playButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.playButton.Image = ((System.Drawing.Image)(resources.GetObject("playButton.Image")));
            this.playButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(23, 22);
            this.playButton.Text = "toolStripButton2";
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // pauseButton
            // 
            this.pauseButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pauseButton.Image = ((System.Drawing.Image)(resources.GetObject("pauseButton.Image")));
            this.pauseButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(23, 22);
            this.pauseButton.Text = "toolStripButton3";
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(1904, 24);
            this.mainMenu.TabIndex = 1;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MidiToolStripMenuItem});
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.importToolStripMenuItem.Text = "Import";
            // 
            // MidiToolStripMenuItem
            // 
            this.MidiToolStripMenuItem.Name = "MidiToolStripMenuItem";
            this.MidiToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.MidiToolStripMenuItem.Text = "MIDI";
            this.MidiToolStripMenuItem.Click += new System.EventHandler(this.MidiToolStripMenuItem_Click);
            // 
            // TabEditor
            // 
            this.TabEditor.AutoScroll = true;
            this.TabEditor.BackColor = System.Drawing.SystemColors.ControlDark;
            this.TabEditor.Location = new System.Drawing.Point(13, 53);
            this.TabEditor.Name = "TabEditor";
            this.TabEditor.Size = new System.Drawing.Size(1879, 774);
            this.TabEditor.TabIndex = 2;
            // 
            // songInfo
            // 
            this.songInfo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.songInfo.Controls.Add(this.fileTempo);
            this.songInfo.Controls.Add(this.fileLength);
            this.songInfo.Controls.Add(this.tempo);
            this.songInfo.Controls.Add(this.SongLength);
            this.songInfo.Controls.Add(this.FileName);
            this.songInfo.Location = new System.Drawing.Point(13, 833);
            this.songInfo.Name = "songInfo";
            this.songInfo.Size = new System.Drawing.Size(1879, 196);
            this.songInfo.TabIndex = 3;
            // 
            // SongLength
            // 
            this.SongLength.AutoSize = true;
            this.SongLength.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SongLength.Location = new System.Drawing.Point(3, 30);
            this.SongLength.Name = "SongLength";
            this.SongLength.Size = new System.Drawing.Size(141, 21);
            this.SongLength.TabIndex = 1;
            this.SongLength.Text = "Song Length: ";
            // 
            // FileName
            // 
            this.FileName.AutoSize = true;
            this.FileName.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileName.Location = new System.Drawing.Point(3, 9);
            this.FileName.Name = "FileName";
            this.FileName.Size = new System.Drawing.Size(118, 21);
            this.FileName.TabIndex = 0;
            this.FileName.Text = "File Name: ";
            // 
            // tempo
            // 
            this.tempo.AutoSize = true;
            this.tempo.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tempo.Location = new System.Drawing.Point(3, 51);
            this.tempo.Name = "tempo";
            this.tempo.Size = new System.Drawing.Size(138, 21);
            this.tempo.TabIndex = 2;
            this.tempo.Text = "Song Tempo: ";
            // 
            // fileLength
            // 
            this.fileLength.AutoSize = true;
            this.fileLength.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileLength.Location = new System.Drawing.Point(147, 30);
            this.fileLength.Name = "fileLength";
            this.fileLength.Size = new System.Drawing.Size(68, 21);
            this.fileLength.TabIndex = 0;
            this.fileLength.Text = "label1";
            // 
            // fileTempo
            // 
            this.fileTempo.AutoSize = true;
            this.fileTempo.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileTempo.Location = new System.Drawing.Point(147, 51);
            this.fileTempo.Name = "fileTempo";
            this.fileTempo.Size = new System.Drawing.Size(68, 21);
            this.fileTempo.TabIndex = 3;
            this.fileTempo.Text = "label1";
            // 
            // PersonalTabEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.songInfo);
            this.Controls.Add(this.TabEditor);
            this.Controls.Add(this.playMenu);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "PersonalTabEditor";
            this.Text = "Personal Tab Editor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.playMenu.ResumeLayout(false);
            this.playMenu.PerformLayout();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.songInfo.ResumeLayout(false);
            this.songInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip playMenu;
        private System.Windows.Forms.ToolStripButton stopButton;
        private System.Windows.Forms.ToolStripButton playButton;
        private System.Windows.Forms.ToolStripButton pauseButton;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Panel TabEditor;
        private System.Windows.Forms.Panel songInfo;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MidiToolStripMenuItem;
        private System.Windows.Forms.Label SongLength;
        private System.Windows.Forms.Label FileName;
        private System.Windows.Forms.Label tempo;
        private System.Windows.Forms.Label fileTempo;
        private System.Windows.Forms.Label fileLength;
    }
}

