﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace GuitarTabEditor
{
    class FileManager
    {
        private OpenFileDialog ofd;
        private AudioManager ad;
        public FileManager()
        {
            ofd = new OpenFileDialog();
            ad = new AudioManager();
        }

        public bool openMidi()
        {
            ofd.Filter = "MIDI Files (*.midi; *.mid) | *.midi; *.mid";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                return ad.Open(ofd.FileName);
            }
            return false;
        }
    }
}
